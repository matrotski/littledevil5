﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    public class SceneController: BaseController
    {
        public List<GameObject> Vertexes { get; set; }
        public List<int> ItemsIndexes { get; set; }
        public List<GameObject> GetVertexObjects()
        {
            var vertexes = GameObject.FindGameObjectsWithTag(BaseConstants.GraphVertexTag)?.ToList();
            var enemies = GameObject.FindGameObjectsWithTag(BaseConstants.EnemyTag)?.ToList();
            var items = GameObject.FindGameObjectsWithTag(BaseConstants.ItemTag)?.ToList();
            Vertexes = new List<GameObject>();
            Vertexes.AddRange(vertexes);
            Vertexes.AddRange(enemies);
            Vertexes.AddRange(items);
            return Vertexes;
        }

        public List<int> GetItemsIndexes()
        {
            var itemsIndexes = new List<int>();
            for (var i = 0; i < Vertexes.Count; ++i)
            {
                Vertexes[i].TryGetComponent(out BaseViewModel viewModel);
                if (viewModel?.AdditionalTag == BaseConstants.ItemTag) itemsIndexes.Add(i);
            }
            ItemsIndexes = itemsIndexes;
            return ItemsIndexes;
        }

        public void RemoveVertex(GameObject item)
        {
            Vertexes.Remove(item);
        }

        public void AddVertex(GameObject item)
        {
            Vertexes.Add(item);
        }
    }
}
