﻿using Assets.Scripts.Controllers;
using UnityEngine;
using UnityEngine.EventSystems;

public class CrushItemController : BaseController
{
    public SceneController SceneController;

    private const float delay = 0.1f;
    
    public void СrushItem(Collider2D collider)
    {
        BaseViewModel viewModel = null;
        collider.gameObject.TryGetComponent<BaseViewModel>(out viewModel);
        if (viewModel != null)
        {
            if (viewModel.AdditionalTag == BaseConstants.ItemTag)
            {
                collider.isTrigger = true;
                SceneController.RemoveVertex(collider.gameObject);
            }
        }
    }
}

