﻿using Assets.Scripts;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class CatchController : BaseController
{
    public GameObject Place;
    private Action _stopAction;
    private Action _actionAfterSetToPlace = null;
    
    public GameObject CatchedObject { get; private set; } = null;

    public void Init (Action stopAction, Action actionAfterSetToPlace = null)
    {
        _stopAction = stopAction;
        _actionAfterSetToPlace = actionAfterSetToPlace;
    }

    public void OnTapItem(BaseEventData pointerEventData)
    {
        var ped = (pointerEventData as PointerEventData);
        if (ped != null)
        {
            Global.CatchedObject = ped.pointerCurrentRaycast.gameObject;
            CatchItem();
        }
    }

    public void OnTapPlace(BaseEventData pointerEventData)
    {
        var ped = (pointerEventData as PointerEventData);
        
        if (ped != null && Global.CatchedObject != null)
        {
            SetToPlace(ped.pointerCurrentRaycast.gameObject);
            _actionAfterSetToPlace?.Invoke();
            Global.CatchedObject = null;
        }
    }

    private void CatchItem()
    {
        _stopAction.Invoke();
        Place.SetActive(true);
    }

    private void SetToPlace(GameObject tapedGO)
    {
        if (Place == tapedGO)
        {
            var clickObjectPosition = tapedGO.transform.position;
            transform.position = new Vector3(clickObjectPosition.x, clickObjectPosition.y, 0);
            Place.SetActive(false);
        }
    }
}

