﻿using UnityEngine;

public class DeleteItemController : MonoBehaviour
{
    private const float delay = 0.1f;

    public void OnDeleteItem(Collider2D collider)
    {
        if (collider != null)
        {
            if (collider.tag != BaseConstants.EnemyTag)
            {
                collider.isTrigger = false;
                Destroy(collider.gameObject, delay);
            }
        }
    }
}

