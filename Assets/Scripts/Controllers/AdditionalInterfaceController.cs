﻿using UnityEngine;
using UnityEngine.EventSystems;

public class AdditionalInterfaceController : BaseController
{
    private const float delay = 0.1f;

    public void OnTapItemToDelete(BaseEventData pointerEventData)
    {
        var ped = (pointerEventData as PointerEventData);
        if (ped != null)
              Destroy(ped.pointerCurrentRaycast.gameObject, delay);
    }
}

