﻿using UnityEngine;

namespace Assets.Scripts.Controllers.UI
{
    public class ScoreController: BaseController
    {
        private float startWidth;
        private float currentWidth;
        private RectTransform rectTransform;
        private void Start()
        {
            rectTransform = GetComponent<RectTransform>();
            startWidth = rectTransform.rect.width;
            currentWidth = startWidth;
        }

        public void SetScore(int score)
        {
            var deltaWidth = score * startWidth / BaseConstants.TotalScore;
            currentWidth += deltaWidth;
            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, currentWidth);
        }
    }
}
