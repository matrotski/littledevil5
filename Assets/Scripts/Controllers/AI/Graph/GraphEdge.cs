﻿/// <summary>
/// Ребро графа
/// </summary>
public class GraphEdge<T> where T : class
{
    /// <summary>
    /// Связанная вершина
    /// </summary>
    public GraphVertex<T> ConnectedVertex { get; }

    /// <summary>
    /// Вес ребра
    /// </summary>
    public int EdgeWeight { get; }

    /// <summary>
    /// Конструктор
    /// </summary>
    /// <param name="connectedVertex">Связанная вершина</param>
    /// <param name="weight">Вес ребра</param>
    public GraphEdge(GraphVertex<T> connectedVertex, int weight)
    {
        ConnectedVertex = connectedVertex;
        EdgeWeight = weight;
    }
}

