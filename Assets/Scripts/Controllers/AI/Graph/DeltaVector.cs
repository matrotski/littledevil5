﻿using System;

public class DeltaVector
{
    public VPoint CountDelta(GraphVertex<VPoint> startPoint1, GraphVertex<VPoint> endPoint1, float deltaLength)
    {
        var startPoint = startPoint1.Value;
        var endPoint = endPoint1.Value;
        var vector = new VPoint();
        vector.X = endPoint.X - startPoint.X;
        vector.Y = endPoint.Y - startPoint.Y;

        var epsilon = Math.Abs(0.07);

        if (Math.Abs(vector.X) <= epsilon) vector.X = 0;
        if (Math.Abs(vector.Y) <= epsilon) vector.Y = 0;

        var vectorLength = Math.Sqrt(vector.X * vector.X + vector.Y * vector.Y);
        var k = (float)vectorLength / deltaLength;
        var delta = new VPoint();
        delta.X = k == 0 ? vector.X : vector.X / k;
        delta.Y = k == 0 ? vector.Y : vector.Y / k;
        return delta;
    }
}

