﻿using Assets.Scripts.ViewModels;
using System.Collections.Generic;
using UnityEngine;

public class AIGraph
{
    public Graph<VPoint> Graph { get; set; }
    public AIGraph(List<GameObject> vertexObjects)
    {
        Graph = new Graph<VPoint>();

        for (var i = 0; i < vertexObjects.Count; ++i)
            Graph.AddVertex(vertexObjects[i].name.ToString(), new VPoint { X = vertexObjects[i].transform.position.x, Y = vertexObjects[i].transform.position.y });

        for (var i = 0; i < vertexObjects.Count; ++i)
        {
            var vm = vertexObjects[i].GetComponent<VertexViewModel>();
            if (vm?.Left != null)
                Graph.AddEdge(vertexObjects[i].name.ToString(), vm?.Left.name.ToString(), 1);
            if (vm?.Right != null)
                Graph.AddEdge(vertexObjects[i].name.ToString(), vm?.Right.name.ToString(), 1);
            if (vm?.Top != null)
                Graph.AddEdge(vertexObjects[i].name.ToString(), vm?.Top.name.ToString(), 1);
            if (vm?.Bottom != null)
                Graph.AddEdge(vertexObjects[i].name.ToString(), vm?.Bottom.name.ToString(), 1);
        }
    }

    public List<GraphVertex<VPoint>> FindShortestPath(VPoint start, VPoint end)
    {
        var dijkstra = new Dijkstra<VPoint>(Graph);
        return dijkstra.FindShortestPath(start, end);
    }
}

