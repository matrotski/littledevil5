﻿public class VPoint
{
    public float X { get; set; }
    public float Y { get; set; }

    public override bool Equals(object point)
    {
        return X == (point as VPoint).X && Y == (point as VPoint).Y;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}
