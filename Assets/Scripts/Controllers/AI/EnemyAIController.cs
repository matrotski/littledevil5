﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyAIController
{
    public List<(GraphVertex<VPoint> vertex, VPoint delta)> GetPathAndDeltas(List<GameObject> graphVertexItems, int startIndex, int endIndex)
    {
        if (startIndex != -1 && endIndex != -1)
        {
            var graph = new AIGraph(graphVertexItems);
            var startPoint = new VPoint { X = graphVertexItems[startIndex].transform.position.x, Y = graphVertexItems[startIndex].transform.position.y };
            var endPoint = new VPoint { X = graphVertexItems[endIndex].transform.position.x, Y = graphVertexItems[endIndex].transform.position.y };

            var path = graph.FindShortestPath(startPoint, endPoint);
            var deltaVector = new DeltaVector();
            var tuples = new List<(GraphVertex<VPoint> vertex, VPoint delta)>();
            if (path.Count > 1)
                tuples.Add((vertex: path[0], delta: deltaVector.CountDelta(path[0], path[1], BaseConstants.EnemySpeedOld)));
            for (var i = 1; i < path.Count - 1; ++i)
            {
                var delta = deltaVector.CountDelta(path[i], path[i + 1], BaseConstants.EnemySpeedOld);
                tuples.Add((vertex: path[i], delta: delta));
            }
            tuples.Add((path[path.Count - 1], new VPoint { X = 0, Y = 0 }));
            return tuples;
        }
        return null;
    }
}
