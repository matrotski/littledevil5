﻿using Assets.Scripts.Controllers;
using Assets.Scripts.ViewModels;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(SceneController))]
    public class EnemiesController: BaseController
    {
        private SceneController sceneController;
        private int countEnemies = 0;

        public GameObject CreatedObject;

        private void Start()
        {
            sceneController = GetComponent<SceneController>();
        }

        public void Update()
        {
            if (Condition())
            {
                CreateEnemy();
            }
        }

        private bool Condition()
        {
            Debug.Log($"{ countEnemies } { sceneController.ItemsIndexes.Count }");
            var firstCondition = sceneController.ItemsIndexes != null && countEnemies < sceneController.ItemsIndexes.Count;
            var allowCreate = Random.Range(0, 5000) < 5;
            return firstCondition && allowCreate;
        }

        #region Creating Enemies
        public void CreateEnemy()
        {
            var coordinates = GetCoordinatesForCreation();
            Instantiate(CreatedObject, new Vector3(coordinates.x, coordinates.y, 0), Quaternion.identity);
            countEnemies++;
        }

        private (float x, float y) GetCoordinatesForCreation()
        {
            var vertexes = sceneController.GetVertexObjects();
            var edges = GetAllEdges(vertexes);
            var edgeIndex = Random.Range(0, edges.Count);
            var start = edges[edgeIndex].Value;
            var endIndex = Random.Range(0, edges[edgeIndex].Edges.Count);
            var end = edges[edgeIndex].Edges[endIndex].ConnectedVertex.Value;

            //CreatedObject.TryGetComponent(out VertexViewModel createdObjectVertexVM);
            //if (createdObjectVertexVM != null)
            //{
            //    vertexes[edgeIndex].TryGetComponent(out VertexViewModel startVertexVM);
            //    vertexes[endIndex].TryGetComponent(out VertexViewModel endVertexVM);
            //    createdObjectVertexVM.Left = startVertexVM.Right == vertexes[endIndex] ? startVertexVM.Right : null;
            //    createdObjectVertexVM.Right = startVertexVM.Right == endVertexVM.Left ? startVertexVM.Left : null;
            //}
            return (Random.Range(start.X, end.X), Random.Range(start.Y, end.Y));
        }

        private List<GraphVertex<VPoint>> GetAllEdges(List<GameObject> graphVertexItems)
        {
            var graph = new AIGraph(graphVertexItems);
            return graph.Graph.Vertices;//list;
        }

        #endregion
    }
}
