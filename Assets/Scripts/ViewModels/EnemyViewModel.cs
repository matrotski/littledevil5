﻿using Assets.Scripts.Controllers;
using Assets.Scripts.Controllers.UI;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Assets.Scripts.ViewModels;

public class EnemyViewModel : BaseViewModel
{
    public DeleteItemController DeleteItemController;
    public ScoreController ScoreController;
    private EnemyAIController enemyAIController;
    public SceneController sceneController;

    private CrushItemController _destroyItemController = null;
    private CatchController _catchController = null;

    private Rigidbody2D _rigidbody;

    private const float delay = 0.1f;

    private int previosEndIndex = -1;
    private List<GameObject> Vertexes;
    private VertexViewModel VertexVm;

    private List<(GraphVertex<VPoint> vertex, VPoint delta)> pathAndDeltas;
    int nodeIndex = 0;

    private void Start()
    {
        enemyAIController = new EnemyAIController();
        Vertexes = sceneController.GetVertexObjects();
        if (previosEndIndex != -1)
            VertexVm = Vertexes[previosEndIndex].GetComponent<VertexViewModel>();
        pathAndDeltas = SetPathAndDeltas(Vertexes);
        TryGetComponent(out _destroyItemController);
        TryGetComponent(out _catchController);
        TryGetComponent(out _rigidbody);
        _catchController?.Init(StopFunc, DeleteItem);
        StartMoving();
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (Vertexes != null && Vertexes.Any())
        {
            var vertexNumb = FindVertexInList(collider.gameObject);
            if (vertexNumb != -1)
            {
                collider.gameObject.TryGetComponent(out BaseViewModel vm);
                if (nodeIndex < pathAndDeltas.Count - 1)
                {
                    if (vm == null || (vm != null && vm.AdditionalTag != BaseConstants.ItemTag))
                    {
                        StopMoving();
                        nodeIndex++;
                        transform.position = new Vector2(pathAndDeltas[nodeIndex].vertex.Value.X, pathAndDeltas[nodeIndex].vertex.Value.Y);
                        StartMoving();
                    }
                    else if (vm != null && vm.AdditionalTag == BaseConstants.ItemTag) nodeIndex++; 
                }

                if (nodeIndex == pathAndDeltas.Count - 1)
                {
                    _destroyItemController.СrushItem(collider);
                    if (vm != null)
                        vm.AdditionalTag = string.Empty;
                    transform.position = new Vector2(pathAndDeltas[nodeIndex].vertex.Value.X, pathAndDeltas[nodeIndex].vertex.Value.Y);
                    SetEndAction();
                }
            }
        }
    }

    private int FindVertexInList(GameObject v)
    {
        return Vertexes.IndexOf(v);
    }

    private void StartMoving()
    {
        var vector = new Vector2(pathAndDeltas[nodeIndex].delta.X * BaseConstants.EnemySpeed, pathAndDeltas[nodeIndex].delta.Y * BaseConstants.EnemySpeed);
        _rigidbody.AddForce(vector);
    }

    private void StopMoving()
    {
        _rigidbody.velocity = new Vector2(0, 0);
    }

    private void SetEndAction()
    {
        Vertexes = sceneController.GetVertexObjects();
        if (previosEndIndex != -1)
            VertexVm = Vertexes[previosEndIndex].GetComponent<VertexViewModel>();
        pathAndDeltas = SetPathAndDeltas(Vertexes);

        if (pathAndDeltas != null)
        {
            if (VertexVm != null)
                VertexVm.Action();
            nodeIndex = 0;
            StopMoving();
            StartMoving();
        }
        else
            StopMoving();

    }

    private List<(GraphVertex<VPoint> vertex, VPoint delta)> SetPathAndDeltas(List<GameObject> vertexes)
    {
        var enemy = GetRandomEnemy();//GameObject.Find("Enemy");
        var startIndex = vertexes.IndexOf(enemy);
        var enemyVM = enemy.GetComponent<VertexViewModel>();
        if (VertexVm != null && enemyVM != null)
        {
            enemyVM.Left = null;
            enemyVM.Right = null;
            enemyVM.Top = null;
            enemyVM.Bottom = null;

            enemyVM.Left = VertexVm.Left;
            enemyVM.Right = VertexVm.Right;
            enemyVM.Top = VertexVm.Top;
            enemyVM.Bottom = VertexVm.Bottom;
        }
        var endIndex = GetRandomItemIndex();
        previosEndIndex = endIndex;
        return enemyAIController?.GetPathAndDeltas(vertexes, startIndex, endIndex);
    }

    private GameObject GetRandomEnemy()
    {
        //var itemsIndexes = sceneController.GetItemsIndexes();
        //if (itemsIndexes.Count == 0) return -1;
        var enemies = GameObject.FindGameObjectsWithTag("Enemy");
        return enemies != null && enemies.Any() ? enemies[Random.Range(0, enemies.Length)] : null;
    }

    private int GetRandomItemIndex()
    {
        var itemsIndexes = sceneController.GetItemsIndexes();
        if (itemsIndexes.Count == 0) return -1;
        return itemsIndexes[Random.Range(0, itemsIndexes.Count)];
    }

    private void StopFunc()
    {
        StopMoving();
    }

    private void DeleteItem()
    {
        DeleteItemController?.OnDeleteItem(GetComponent<Collider2D>());
        ScoreController.SetScore(BaseConstants.CatchEnemyScore);
    }
}




