﻿using Assets.Scripts.Controllers;
using System;
using UnityEngine;

public class ItemViewModel : BaseViewModel
{
    public SceneController SceneController;

    private CatchController _controller = null;

    private void Awake()
    {
        AdditionalTag = BaseConstants.ItemTag;
        TryGetComponent(out _controller);
        //var stopFuncAction = new Action(StopFunc);
        _controller?.Init(StopFunc, AfterSetPlace);
    }

    public void StopFunc()
    {
        TryGetComponent(out Rigidbody2D gameObjectRigidbody);
        if (gameObjectRigidbody != null)
        {
            gameObjectRigidbody.velocity = Vector2.zero;
            gameObjectRigidbody.gravityScale = 0;
        }
    }

    public void AfterSetPlace()
    {
        //AdditionalTag = BaseConstants.ItemTag;
        tag = BaseConstants.GraphVertexTag;
        SceneController.AddVertex(this.gameObject);
    }
}
