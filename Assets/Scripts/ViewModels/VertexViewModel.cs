﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.ViewModels
{
    public class VertexViewModel: MonoBehaviour
    {
        public GameObject Left = null;
        public GameObject Right = null;
        public GameObject Top = null;
        public GameObject Bottom = null;

        public virtual IEnumerator Action()
        {
            yield return new WaitForSeconds(2.0f);
        }
    }
}
