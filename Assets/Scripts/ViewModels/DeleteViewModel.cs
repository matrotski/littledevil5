﻿using Assets.Scripts.Controllers.UI;
using UnityEngine;

public class DeleteViewModel : BaseViewModel
{
    public DeleteItemController DeleteItemController;
    public ScoreController ScoreController;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        DeleteItemController.OnDeleteItem(collider);
        ScoreController.SetScore(BaseConstants.CrushItemScore);
    }
    void OnCollisionEnter2D(Collision2D coll)
    {
        DeleteItemController.OnDeleteItem(coll.collider);
        ScoreController.SetScore(BaseConstants.CrushItemScore);
    }
}
