﻿using UnityEngine;

public class BaseConstants : MonoBehaviour
{
    public const string ItemTag = "Item";
    public const string GraphVertexTag = "GraphVertex";
    public const string EnemyTag = "Enemy";

    public const int TotalScore = 100;

    public const float EnemySpeedOld = 0.01f;
    public const float EnemySpeed = 5000f;

    public const int CrushItemScore = -5;
    public const int CatchEnemyScore = 1;
}
